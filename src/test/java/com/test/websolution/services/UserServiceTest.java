package com.test.websolution.services;

import com.test.websolution.dto.UserDto;
import com.test.websolution.model.Users;
import com.test.websolution.repository.UsersRepository;
import com.test.websolution.security.services.UserDetailsImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.test.context.event.annotation.BeforeTestClass;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@SpringBootTest
@Slf4j
class UserServiceTest {

    @MockBean
    UsersRepository usersRepository;

    @Autowired
    UserService userService;

    @Mock
    private Authentication auth;


    @BeforeAll
    static void beforeAll() {
     /*   log.info("Test class started");
        Set<GrantedAuthority> authorities = new HashSet<>();


        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(new UserDetailsImpl(2L,
                "user1","www.user1.com",
                "hkjo8G",
                authorities), null));
        SecurityContextHolder.setContext(securityContext);*/

    }

    @BeforeEach
    public void initSecurityContext() {
        Set<GrantedAuthority> authorities = new HashSet<>();


        SecurityContextImpl securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(new UserDetailsImpl(2L,
                "user1","www.user1.com",
                "hkjo8G",
                authorities), null));
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    public void findByUrl() throws Exception {

        log.info("Testing findByUrl");

        Users mockUser = new Users(1L, "Sam", "sam.assda.asdas", "pop", "john");
        Optional<Users> optionalUsers = Optional.of(mockUser);
        when(this.usersRepository.findByUrlAndCreatedByUsername("www.user10.com", "user1")).thenReturn(optionalUsers);
        Users users = userService.findByUrl("www.user10.com");
        Assertions.assertNotNull(users, "User is null");
        Assertions.assertEquals(optionalUsers.get().getUsername(), users.getUsername(), "Username does not match");
    }

    @Test
    public void findByCreatedByUsername() {
        Users mockUser1 = new Users(1L, "Sam", "sam.assda.asdas", "poewrewp", "john");
        Users mockUser2 = new Users(2L, "Ram", "ram.assda.asdas", "fds", "john");
        Users mockUser3 = new Users(3L, "Jam", "jam.assda.asdas", "fsdf", "john");
        Users mockUser4 = new Users(4L, "Kam", "kam.assda.asdas", "podsfsdp", "john");
        when(this.usersRepository.findByCreatedByUsername("john")).thenReturn(Arrays.asList(mockUser1, mockUser2, mockUser3, mockUser4));
        List<Users> usersList = userService.findByCreatedByUsername();
        Assertions.assertNotNull(usersList, "Userlist is null");
       // Assertions.assertEquals(4, usersList.size(), "findByCreatedByUsername should return 4 users");

    }



    @AfterEach
    public void clearSecurityContext() {
        SecurityContextHolder.clearContext();
    }
}