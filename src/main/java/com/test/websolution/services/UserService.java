package com.test.websolution.services;

import com.test.websolution.dto.UserDto;
import com.test.websolution.model.Users;
import com.test.websolution.repository.UsersRepository;
import com.test.websolution.security.services.UserDetailsImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserService {

    @Autowired
    UsersRepository userRepository;

    /*@Autowired
    PasswordEncoder encoder;*/


    public Users addUser(UserDto userDto, String password) {

        // Create new user's account
        log.info("Authentication {}", SecurityContextHolder.getContext().getAuthentication());
        log.info("Principal {}", SecurityContextHolder.getContext().getAuthentication().getPrincipal());


        String createdByUsername = null;
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDetailsImpl) {
            createdByUsername = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        }
        Users user = new Users(userDto.getUsername(),
                userDto.getUrl(), password, createdByUsername);

        return userRepository.save(user);
    }

    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    public Users findByUrl(String url) throws Exception {
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<Users> optionalUsers = userRepository.findByUrlAndCreatedByUsername(url, userDetails.getUsername());
        if (!optionalUsers.isPresent()) {
            throw new Exception("User not found");
        }
        return optionalUsers.get();
    }

    public List<Users> findByCreatedByUsername() {
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findByCreatedByUsername(userDetails.getUsername());
    }
}
