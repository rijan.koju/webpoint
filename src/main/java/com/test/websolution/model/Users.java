package com.test.websolution.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "url")
        })
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String url;

    private String password;

    private String createdByUsername;

    public Users(String username, String url, String password) {
        this.username = username;
        this.url = url;
        this.password = password;
    }

    public Users(String username, String url, String password,String createdByUsername) {
        this.username = username;
        this.url = url;
        this.password = password;
        this.createdByUsername = createdByUsername;
    }


}