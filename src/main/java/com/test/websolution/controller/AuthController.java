package com.test.websolution.controller;


import com.test.websolution.dto.JwtResponse;
import com.test.websolution.dto.LoginDto;
import com.test.websolution.dto.MessageResponse;
import com.test.websolution.dto.UserDto;
import com.test.websolution.security.jwt.JwtUtils;
import com.test.websolution.security.services.UserDetailsImpl;
import com.test.websolution.services.UserService;
import com.test.websolution.util.RandomCharGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtils jwtUtils;


    @PostMapping("/login")
    public MessageResponse<JwtResponse> authenticateUser(@RequestBody LoginDto loginDto) {


        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        return new MessageResponse<>(new JwtResponse(jwt, new UserDto(userDetails.getUsername(), userDetails.getUrl())), HttpStatus.OK);
    }

    @PostMapping("/signup")
    public MessageResponse<String> registerUser(@RequestBody UserDto userDto) {
        if (userService.existsByUsername(userDto.getUsername())) {
            return new MessageResponse<>("Error: Username is already taken!", HttpStatus.BAD_REQUEST);
        }

        String password = RandomCharGenerator.getAlphaNumericString(6);
        userService.addUser(userDto, password);
        return new MessageResponse<>("Sign up successful! Your password is " + password, HttpStatus.OK);
    }
}