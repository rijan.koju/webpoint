package com.test.websolution.controller;

import com.test.websolution.dto.MessageResponse;
import com.test.websolution.dto.UserDto;
import com.test.websolution.dto.UserList;
import com.test.websolution.model.Users;
import com.test.websolution.services.UserService;
import com.test.websolution.util.RandomCharGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/users")
public class UsersController {

    @Autowired
    private UserService userService;

    @GetMapping("/search")
    public MessageResponse<Users> searchByUrl(@RequestParam("url") String url) {
        try {
            Users users = userService.findByUrl(url);
            return new MessageResponse<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new MessageResponse<>(e.getMessage(), HttpStatus.NOT_FOUND);

        }
    }

    @PostMapping("/add")
    public MessageResponse<Users> addUser(@RequestBody UserDto userDto) {
        if (userService.existsByUsername(userDto.getUsername())) {
            return new MessageResponse<>("Error: Username is already taken!", HttpStatus.BAD_REQUEST);
        }
        String password = RandomCharGenerator.getAlphaNumericString(6);
        return new MessageResponse<>(userService.addUser(userDto, password), HttpStatus.OK);

    }


    @GetMapping("/all")
    public MessageResponse<UserList> getUserList() {
        List<Users> usersList = userService.findByCreatedByUsername();
        return new MessageResponse<>(new UserList(usersList), HttpStatus.OK);
    }


}
