package com.test.websolution.repository;

import com.test.websolution.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users,Long> {

    Optional<Users> findByUsername(String username);

    Boolean existsByUsername(String username);

    Optional<Users> findByUrlAndCreatedByUsername(String url, String createdByUsername);

    List<Users> findByCreatedByUsername(String createdByUsername);

}
