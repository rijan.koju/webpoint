package com.test.websolution.dto;

import com.test.websolution.model.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponse {

    private String token;

    private String type = "Bearer";

    private UserDto userDto;

    public JwtResponse(String accessToken, UserDto userDto) {
        this.token = accessToken;
        this.userDto = userDto;
    }
}