package com.test.websolution.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MessageResponse<T> {
    private T entity;

    private String message;

    private HttpStatus httpStatus;

    public MessageResponse(T entity, HttpStatus httpStatus) {
        this.entity = entity;
        this.httpStatus = httpStatus;
    }

    public MessageResponse(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

}