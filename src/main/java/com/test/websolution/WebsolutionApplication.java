package com.test.websolution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsolutionApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebsolutionApplication.class, args);
	}

}
